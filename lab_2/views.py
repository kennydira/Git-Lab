from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Halo semuanya, selamat datang di Landing Page yang telah saya buat di kelas PPW - D. Page ini dibuat dengan segala kepusingan yang ada, apalagi charger laptop rusak jadi makin pusing kalau laptop  mati. Mohon maaf atas ketidaknyamanan yang Anda alami. Selamat beraktivitas. Terima kasih.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
