Tutorials and Assignment Repository

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018




Table of Contents

Welcome to the code repository.
This repository hosts weekly tutorial codes and other, such as course-related
code snippets.


Weekly Exercises



Lab 1 - Introduction to Git (on GitLab) & TDD (Test-Driven Development) with Django

Lab 2 - Introduction to Django Framework

Lab 3 - Introduction to Models Django and Heroku Database with TDD Discipline

Lab 4 - Pengenalan HTML5


Lab 5 - Pengenalan CSS


Lab 6 - Pengenalan Javascript dan JQuery


Lab 7 - Pengenalan Web Service


Lab 8 - Pengenalan Pengenalan Oauth2


Lab 9 - Oauth, Webservice, beserta Pengenalan Cookie dan Session


Lab 10 - Penerapan Lanjut Cookie dan Session



Quickstart Guide
Initial Setup
Doing the Tutorial
Pulling Updates From Upstream
Show Code Coverage in Gitlab
Grading Scheme & Demonstration



TL;DR

After you work at Lab 1, make sure to link this repository to your Lab 1 Repository :


Add this repository link to your remote list as upstream (git remote add upstream https://gitlab.com/PPW-2017/ppw-lab)
Pull the latest update to check whether new tutorials have been updated (git pull upstream master)
Fix any merge conflict(s) that might arise (hopefully none)


Always choose latest commit from upstream when fixing merge
conflict(s)


Do not forget to commit your merged master branch and push it
to your own master branch at GitLab repository


Use Git command: git push origin master





Working on a tutorial problem set (This instructions applied for 3rd tutorials and so on):


Pull any updates from upstream

Create new apps on Django Project based on your tutorials python manage.py startapp lab_n where n is turoial number. E.g. lab_2

Do the exercises as instructed in README.md file (click this to see this week Tutorials README.md)
Commit your work frequently
Write good commit message(s)
If your work is ready for grading: git push origin master



If you want to know the detailed explanation about each instructions above,
please read the following sections.


Initial Setup

If you previously haven't worked on Lab 1 Tutorial


then Create a fork of this repository to your GitLab account, which
will create a copy of this repository under your own account.
Open the forked repository page at
https://gitlab.com/<YOURNAME>/ppw-lab where <YOURNAME>
is your GitLab username.
Set the clone URL to HTTPS and copy the URL into clipboard.
Clone the repository into your local machine. Use Git command:
git clone https://gitlab.com/<YOURNAME>/ppw-lab.git <PATH>
where <PATH> is a path to a directory in your local machine.
Go to the directory where the cloned repository is located in your
local machine.
Add new remote called upstream that points to the original
GitLab repository. Use Git command: git remote add upstream https://gitlab.com/PPW-2017/ppw-lab

Tell your TA about your GitLab username and URL to your tutorial
repository so s/he can grade it later.
Ensure that your repository page has visibility level set to
Internal or Public. Check it in Edit Project menu at
your repository page.


If you did Lab 1 Tutorial


Add new remote called upstream that points to the original
GitLab repository. Use Git command: git remote add upstream git remote add upstream https://gitlab.com/PPW-2017/ppw-lab

Tell your TA about your GitLab username and URL to your tutorial
repository so s/he can grade it later.
Ensure that your repository page has visibility level set to
Internal or Public. Check it in Edit Project menu at
your repository page.



Doing the Tutorial


Suppose that you want to work on Lab 2 problem set. Go to the
directory that containing Lab 2 README.md.
To ensure your work regarding Lab 2 problem is isolated from
your other attempts on other problems, create a new apps
specifically for working on Lab 2 problem. Use Python command:
python manage.py startapp lab_2

Read the README file carefully because It contains set of tasks and instructions that you can work on.
Do the tutorial.
Use git add or git rm to stage/unstage files that you want to
save into Git later.
Once you want to save your progress, commit your work to Git. Use
Git command: git commit A text editor will appear where you should
write a commit message. Please try to follow the guidelines written
in this guide on how to
write a good commit message.
Repeat steps 4 - 6 until you finish the tutorial.
Once you are ready to submit your work or you want to save it to
your repository on GitLab, do a Git push. The Git command:
git push origin master




Pulling Updates From Upstream

If there are any updates from upstream, you can get the latest commits
and integrate it into your fork by using the following Git command:
git pull upstream master

Merge conflicts may arise since the repository is updated weekly and
may have overlapping changes with the master branch in your own
forked repository. If merge conflict happens, please always use latest
commit from upstream. Once you have resolved any merge conflicts and all commits from
upstream are merged succesfully to your own master branch, do not
forget to push it back to your own GitLab repository. Use Git command:
git push origin master


Show Code Coverage in Gitlab


Go to Pipeline Settings (Settings -> Pipelines)
Go to section Coverage Settings (Pipelines -> Test coverage parsing)

Write this Regex (Regular Expression) in textbox Test Coverage Parsing


TOTAL\s+\d+\s+\d+\s+(\d+)%


Now your pipelines page will show your Code Coverage



Grading Scheme & Demonstration

Weekly tutorials contribute 20% to the final grade of this course.
For each exercises, student can obtain grade ranging from A (4) to
E (0). The grading scheme is as follows:



A if student completed all checklists


B if student completed 80% of checklist


C if student completed at least half of the checklist


D if student completed 30% of checklist


E if student skipped the tutorial by doing nothing, e.g.
no signs of work to the tutorial in the repository


All students required to demonstrate their work to teaching assistant.
This demonstration mechanism applies for both students in Regular and
International classes:


Demonstrations should be done no later than the end of the
lab session week. The time allocation for the demonstration can be
adjusted to the availability of the Teaching Assistants. As long as
the demonstration is still done before your lab session, students have the chance
to achieve maximum score for the tutorial.
If the demonstration is done after your lab session, you have to demonstrate
your work to your lecturer and your score won't reach maximum point eventhough you
do all checklists




Happy Coding :)
